#include "play_guess_the_number.hpp"
#include "get_input_from_user.hpp"
#include "random.hpp"

void play_guess_the_number()
{
    int  chosen_int = rand(0, 100);
    int  int_user;
    bool finished = false;
    std::cout << "Guess a number: " << std::endl;
    while (!finished) {
        int_user = get_input_from_user<int>();
        if (int_user == chosen_int) {
            finished = true;
        }
        else {
            if (chosen_int > int_user) {
                std::cout << "Greater" << std::endl;
            }
            else {
                std::cout << "Smaller" << std::endl;
            }
        }
    }
    std::cout << "Congratulations superstar!" << std::endl;
}